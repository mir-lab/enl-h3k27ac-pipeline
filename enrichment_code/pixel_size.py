import pandas as pd
import czifile

LOG_DIRECTORY = './log/'
DATA_FILE = 'data_y27a.csv'
DATA_DIRECTORY = './Data from Lele Song/'
OUTPUT_DIRECTORY = './output/'

data_summary = pd.read_csv(DATA_FILE)

for file_inx in data_summary.index:
        filedir =  data_summary.iloc[file_inx]['Directory']
        filename = data_summary.iloc[file_inx]['Filename']
        print('Processing file: ' + filename)
        IMG = czifile.CziFile(DATA_DIRECTORY + filedir + filename) #open image with metadata
        xy_size = IMG.metadata(raw=False)['ImageDocument']['Metadata']['Scaling']['Items']['Distance'][0]['Value']*1000000
        z_size = IMG.metadata(raw=False)['ImageDocument']['Metadata']['Scaling']['Items']['Distance'][2]['Value']*1000000
        data_summary.at[file_inx, 'XY_Size'] = xy_size
        data_summary.at[file_inx, 'Z_Size'] = z_size
print(data_summary)
data_summary.to_csv('data_y27a_with_pixel_size.csv')