import os
import pandas as pd
import pipeline_utilities
import napari
import matplotlib.pyplot as plt
import numpy as np
import skimage.filters as skfilter
import matplotlib
matplotlib.use('TKAgg')

#CONFIGURATION
data_folder = "./Data from Lele Song"
sub_folders = ['20210609 9-HEK293 plenti-teton-ENL-WT T1 T2 T3 T4 WITH 4DOX-Flag-488 and H3K27ac-568',
               '20210623 9-HEK293-plenti-teton-ENL-cancer mutations T1-T4 with 10dox flag-488 H3K27AC-568',
               'Y27A']

if __name__ == '__main__':
    data_files = pd.DataFrame()
    for subfolder in sub_folders:
        for condition in os.listdir(data_folder+'/'+subfolder):
            for file in os.listdir(data_folder+'/'+subfolder+'/'+condition):
                if file[-3:] != 'czi':
                    continue
                pathname = data_folder+'/'+subfolder+'/'+condition + '/'
                IMG = pipeline_utilities.open_image(pathname+file)
                print(condition, file)
                dox = input("Enter DOX level:> ")
                zoom = input("Enter Zoom:> ")

                happy_x = False
                happy_y = False
                offset_x = 0
                offset_y = 0
                while not happy_x:
                    offset_x = float(input('Enter X offset> '))
                    plt.figure(dpi=200)
                    I = pipeline_utilities.shift_image(IMG, 'X', offset_x)
                    I = I[1,:,:,:]
                    plt.imshow(np.max(I, 1))
                    plt.show()
                    if 'y' in input('Happy?'):
                        happy_x = True

                while not happy_y:
                    offset_y = float(input('Enter Y offset> '))
                    plt.figure(dpi=200)
                    I = pipeline_utilities.shift_image(IMG, 'Y', offset_y)
                    I = I[1,:,:,:]
                    plt.imshow(np.max(I, 2))
                    plt.show()
                    if 'y' in input('Happy?'):
                        happy_y = True

                data_files = data_files.append(pd.DataFrame({
                    'Directory':subfolder + '/' + condition + '/',
                    'Filename':file,
                    'Mutant':condition,
                    'Dox':dox,
                    'Zoom':zoom,
                    'X_Offset':offset_x,
                    'Y_Offset':offset_y,
                    'Pixel_Size':np.nan,
                }, index=[len(data_files)+1]), ignore_index=True)
                data_files.to_csv('data.csv')
    print(data_files)
