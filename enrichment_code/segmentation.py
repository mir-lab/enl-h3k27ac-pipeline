import math
import statistics
from os import makedirs

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import skimage.io as skio
import skimage.morphology as skmorph
import skimage.filters as skfilters
import skimage.segmentation as sksegment
import skimage.measure as skmeasure
from skimage.filters import rank as skrank

from scipy import ndimage as ndimg

import napari

import pipeline_utilities


#maximum_segmentation segments the max projection of a field of nuclei
#expected input is 4D image of dimensions [Channel, Z, X, Y]
def maximum_segmentation(IMG, channel, S1, S2, Percentile, Minsize, Disksize, SR1, SR2, Minarea):
    MAX = np.max(IMG[channel,:], 0) #0 is now dimension for Z
    labels = np.zeros_like(MAX)

    #generate general mask
    mask = np.zeros_like(MAX)
    mask = skfilters.sobel(MAX)
    mask = skfilters.difference_of_gaussians(mask,S1,S2)
    thresh = skfilters.threshold_otsu(mask)
    #thresh = np.percentile(mask, Percentile)
    mask = (mask > thresh).astype(np.uint16)
    mask = ndimg.binary_fill_holes(mask)
    mask = skmorph.remove_small_objects(mask, Minsize)
    
    #generate markers
    markers = np.zeros_like(MAX)
    markers = skfilters.median(MAX, skmorph.disk(Disksize))
    markers = skfilters.difference_of_gaussians(markers, SR1, SR2)
    thresh = skfilters.threshold_otsu(markers)
    markers = (markers > thresh).astype(np.uint16)
    markers = ndimg.binary_fill_holes(markers)
    markers = skmorph.remove_small_objects(markers, Minarea)


    #generate nuclear regions
    regions = sksegment.watershed(1-markers)
    labels = mask * regions
    labels = sksegment.clear_border(labels)

    return labels

def identify_puncta(IMG, channel, mask, percentile, min_area):
    puncta_mask = np.zeros_like(IMG[0,0,:,:])
    MAX = np.max(IMG, 1) #max Z project
    N_nuclei = np.max(mask)
    for i in range(1,N_nuclei+1):
        I = MAX[channel,:,:] * (mask == i)
        thresh = np.percentile(I, percentile)
        I = (I > thresh)
        I = skmorph.remove_small_objects(I, min_area).astype(np.uint16)
        I = ndimg.binary_fill_holes(I)
        puncta_mask += I
    return skmorph.label(puncta_mask)

def segmentation_3d(IMG, channel, mask):
    cylindrical_mask = np.zeros_like(IMG[channel,:])
    final_mask = np.zeros_like(IMG[channel,:])
    for z in range(IMG.shape[1]):
        cylindrical_mask[z,:,:] = mask
    n_nuclei = np.max(mask)
    for n in range(1,n_nuclei+1):
        subimg = skfilters.sobel(IMG[channel,:,:,:]) * (cylindrical_mask == n)
        thresh = skfilters.threshold_mean(subimg)
        subimg = (subimg > thresh).astype(np.uint16) * n
        final_mask += subimg
    return final_mask