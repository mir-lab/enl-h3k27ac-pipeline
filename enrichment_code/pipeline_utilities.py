import math
import statistics
from os import makedirs

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

import czifile
import skimage.io as skio
import skimage.morphology as skmorph
import skimage.filters as skfilters
import skimage.segmentation as sksegment
import skimage.measure as skmeasure
from skimage.filters import rank as skrank

from scipy import ndimage as ndimg

import napari

from random import randint

def crop_image_to_coordinate(IMG, channel, X, Y, Z, lateral_size, axial_size):
    I = IMG[channel, :, :, :]
    out = np.zeros([2*axial_size + 1, 2*lateral_size + 1, 2*lateral_size + 1])
    max_x = I.shape[2] -1
    max_y = I.shape[1] -1
    max_z = I.shape[0] -1

    if X < lateral_size or Y < lateral_size or Z < axial_size:
        return out
    if (X + lateral_size) > max_x or (Y + lateral_size) > max_y or (Z + axial_size) > max_z:
        return out
    return I[Z-axial_size:Z+axial_size+1, Y-lateral_size:Y+lateral_size+1, X-lateral_size:X+lateral_size+1]

def shift_image(i, axis, pixels):
    #dimensions = Channel,Z,Y,X
    if axis == 'X':
        dimension = 3
    else:
        dimension = 2
    half_z = i.shape[1]//2
    cut_size = np.ceil((half_z*np.abs(pixels))).astype(int)
    cut_bound_lower = cut_size
    cut_bound_upper = i.shape[dimension] - cut_size
    if dimension == 3:
        new_image = np.zeros([i.shape[0], i.shape[1], i.shape[2], cut_bound_upper-cut_bound_lower])
    if dimension == 2:
        new_image = np.zeros([i.shape[0], i.shape[1], cut_bound_upper-cut_bound_lower, i.shape[3]])
    for c in range(i.shape[0]):
        for z in range(i.shape[1]):
            if dimension == 3:
                offset = np.round((half_z-z)*pixels).astype(int)
                print(offset, cut_bound_lower+offset, cut_bound_upper+offset)
                new_image[c,z,:,:] = i[c,z,:,cut_bound_lower-offset:cut_bound_upper-offset]
            if dimension == 2:
                offset = np.round((half_z-z)*pixels).astype(int)
                print(offset, cut_bound_lower+offset, cut_bound_upper+offset)
                new_image[c,z,:,:] = i[c,z,cut_bound_lower-offset:cut_bound_upper-offset,:]
    return new_image

def open_image(filename):
    img = czifile.imread(filename)
    #(1, 1, 3, 1, 16, 1024, 1024, 1)
    img = img.reshape((img.shape[2], img.shape[4], img.shape[5], img.shape[6]))
    return img #dimensions of image: [C,Z,Y,X]

def save_image(filename, image):
    skio.imsave(filename, image)

def show_image_2d(data):
    plt.imshow(data)
    plt.show()

def label_puncta(img, nuclei_mask, n_timepoints, method, percentile, value, min_area):
    out = np.zeros(img.shape)
    for t in range(n_timepoints):
        i = img[t,:,:]
        if method == 'Absolute':
            thresh = value
        elif method == 'Percentile':
            thresh = np.percentile(i, percentile)
        mask = (i > thresh)
        mask = skmorph.remove_small_objects(mask, min_area)
        mask = ndimg.binary_fill_holes(mask)
        out[t,:,:] = skmorph.label(nuclei_mask[t,:,:] * mask)
    return out

#generate random coordinate in labeled nucleus
#Image3D of shape [Z,Y,X], binary Mask2D with nucleus labeled as True/1u8
def generate_random_coordinate(Image3D, Mask2D, Punctae, PadLateral=0, PadZ=0, MaxIter=100):
    x = 0
    y = 0
    z = 0
    Mask2D = Mask2D.astype(np.uint8) #ensure interpreted as integer

    #Generate X/Y bounding square on 2D mask [returns (min_row, min_col, max_row, max_col)]
    bounding_box = skmeasure.regionprops(Mask2D)[0].bbox

    #Distance transform
    distances = ndimg.distance_transform_edt(Mask2D)

    #ID x/y coordinates
    Inside_Nucleus = False
    iter = 0
    while not Inside_Nucleus:
        y = randint(bounding_box[0], bounding_box[2])
        x = randint(bounding_box[1], bounding_box[3])
        if (Mask2D[y,x] == 1) and (distances[y,x]  >= PadLateral) and Punctae[y,x] == False:
            Inside_Nucleus = True
        iter += 1
        if iter > MaxIter and Mask2D[y,x] == 1:
            break

    #ID z coordinate
    Z_range = Image3D.shape[0]
    z = randint(0+PadZ, Z_range-PadZ)
    print("Remote site:", x, y, z, '|Bounding Box:', bounding_box)
    return (z,y,x)

def gaussian(x,a,F,offset):
    return a*np.exp(-((x)**2)/(2*((F/2.35)**2)))+offset

def distance_map(img, x,y, pixel_size):
    out = np.zeros(img.shape)
    #find centroid position from 0,0 (upper left corner)
    cx = (x + 0.5)*pixel_size
    cy = (y + 0.5)*pixel_size
    for iy in range(img.shape[0]):
        for ix in range(img.shape[1]):
            dx = (ix + 0.5)*pixel_size
            dy = (iy + 0.5)*pixel_size
            out[iy,ix] = np.sqrt((dx-cx)**2 + (dy-cy)**2)
    return out
    
def radial_profile(img, x, y, pixel_size):
    distances = distance_map(img, x, y, pixel_size)
    D = np.unique(distances)
    M = np.zeros_like(D)
    C = np.zeros_like(D)
    for iy in range(img.shape[0]):
        for ix in range(img.shape[1]):
            M[D == distances[iy,ix]] += img[iy,ix]
            C[D == distances[iy,ix]] += 1
    M = M/C
    return (D,M)

def distance_map_3(img, x, y, z, xy_size, z_size):
    out = np.zeros(img.shape)
    cx = (x + 0.5)*xy_size
    cy = (y + 0.5)*xy_size
    cz = (z + 0.5)*z_size
    for iz in range(img.shape[0]):
        for iy in range(img.shape[1]):
            for ix in range(img.shape[2]):
                dx = (ix + 0.5)*xy_size
                dy = (iy + 0.5)*xy_size
                dz = (iz + 0.5)*z_size
                out[iz,iy,ix] = np.sqrt((dx-cx)**2 + (dy-cy)**2 + (dz-cz)**2)
    return out

def radial_profile_3(img, x, y, z, xy_size, z_size):
    distances = distance_map_3(img, x, y, z, xy_size, z_size)
    D = np.unique(distances)
    M = np.zeros_like(D)
    C = np.zeros_like(D)
    for iz in range(img.shape[0]):
        for iy in range(img.shape[1]):
            for ix in range(img.shape[2]):
                M[D == distances[iz,iy,ix]] += img[iz,iy,ix]
                C[D == distances[iz,iy,ix]] += 1
    M = M/C
    return (D,M)

def normalize_range(Matrix, Max, Min):
    return (Matrix - Min)/(Max-Min)