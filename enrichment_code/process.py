import pipeline_utilities
import segmentation

import numpy as np
import pandas as pd

import skimage.io as skio
import skimage.measure as skmeasure

from os import makedirs
import sys

OUTPUT_DIRECTORY = 'output_with_stats/'
DATA_DIRECTORY = 'Data from Lele Song/'
DATA_FILE = 'data_with_control.csv'

#channels
DAPI_CHANNEL = 0
PUNCTA_CHANNEL = 1
ACETYL_CHANNEL = 2

#puncta crop settings
PUNCTA_RADIUS_LATERAL = 10
PUNCTA_RADIUS_AXIAL = 4

if __name__ == '__main__':
    makedirs(OUTPUT_DIRECTORY, exist_ok=True)

    try:
        data_summary = pd.read_csv(DATA_FILE)
    except:
        sys.exit(1)
    print(data_summary)

    puncta_data = pd.DataFrame()

    for Mutant in data_summary['Mutant'].unique():
        sub1 = data_summary[data_summary['Mutant'] == Mutant]
        for Dox in sub1['Dox'].unique():
            sub2 = sub1[sub1['Dox'] == Dox]
            for XYSize in sub2['XY_Size'].unique():
                subdata = sub2[sub2['XY_Size'] == XYSize]
                pixel_size = np.round(XYSize, 2)
                if pixel_size != 0.07:
                    continue
                condition_name = Mutant + '_' + str(Dox) + "Dox_" + str(pixel_size)[2:] + 'XY'

                print('Condition: ' + condition_name + ', N:' + str(len(subdata)))

                OUT_DIR = OUTPUT_DIRECTORY + 'combined/' + condition_name + '/'
                makedirs(OUT_DIR, exist_ok=True)

                #Data storage for conditon
                PUNCTA_CROPS = []
                ACETYL_CROPS = []
                DAPI_CROPS   = []
                PUNCTA_CROPS_REMOTE = []
                ACETYL_CROPS_REMOTE = []
                DAPI_CROPS_REMOTE = []

                #Iterate over results of each file for condition
                for i, file_inx in enumerate(subdata.index):
                    filedir =  data_summary.iloc[file_inx]['Directory']
                    filename = data_summary.iloc[file_inx]['Filename']
                    IN_DIR = DATA_DIRECTORY + filedir + 'output_' + filename[:-3].strip() + '/'
                    IMG_OUT_DIR = OUTPUT_DIRECTORY + 'single/' + str(file_inx) + '_' + condition_name + '/'
                    makedirs(IMG_OUT_DIR, exist_ok=True)
                    print('Processing file: ' + filename + ' | Index = ' + str(file_inx))

                    #Open image
                    IMG = pipeline_utilities.open_image(DATA_DIRECTORY + filedir + filename)

                    #Apply offset
                    print('Shifting image in Z plane')
                    offset_x = data_summary.iloc[file_inx]['X_Offset']
                    offset_y = data_summary.iloc[file_inx]['Y_Offset']
                    IMG = pipeline_utilities.shift_image(IMG, 'X', offset_x)
                    IMG = pipeline_utilities.shift_image(IMG, 'Y', offset_y)

                    MAX = np.max(IMG, 1) #max z projection
                    print(IMG.shape, MAX.shape)
                    skio.imsave(IMG_OUT_DIR + 'max_z_projection.tif', MAX, check_contrast=False)

                    #Segment Nuclei
                    print('Segmenting Nuclei')
                    MAX_NUC_LABELS = segmentation.maximum_segmentation(
                        IMG, 
                        channel=DAPI_CHANNEL, 
                        S1=3,
                        S2=500, 
                        Percentile=40, 
                        Minsize=5000, 
                        Disksize=1, 
                        SR1=1, 
                        SR2 =35,
                        Minarea=500,
                    )
                    skio.imsave(IMG_OUT_DIR + 'nuclei_labels.tif', MAX_NUC_LABELS, check_contrast=False)

                    #Segment Punctae
                    print('Identifying Punctae')
                    MAX_PUNCTA_LABELS = segmentation.identify_puncta(
                        IMG, 
                        channel=PUNCTA_CHANNEL, 
                        mask=MAX_NUC_LABELS, 
                        percentile=99.99, 
                        min_area=9,
                    )
                    skio.imsave(IMG_OUT_DIR + 'punctae_labels.tif', MAX_PUNCTA_LABELS, check_contrast=False)

                    #Crop punctae
                    puncta_regions = skmeasure.regionprops(MAX_PUNCTA_LABELS)

                    REMOTE_POINT_MAP = np.zeros_like(MAX_NUC_LABELS)

                    #MED = np.median(IMG, 1)

                    for region in puncta_regions:
                        #Identify centroid and nearest pixel
                        (cy,cx) = region.centroid
                        py = np.round(cy).astype(int)
                        px = np.round(cx).astype(int)
                        #Identify Z coordinate
                        pz = 0
                        pzmax = -1
                        for z in range(IMG.shape[1]):
                            if IMG[PUNCTA_CHANNEL,z,py,px] > pzmax:
                                pzmax = IMG[PUNCTA_CHANNEL,z,py,px]
                                pz = z
                        
                        #Identify nucleus
                        nucleus = MAX_NUC_LABELS[py,px]

                        #Set up data arrays

                        if nucleus != 0:
                            #Get single nucleus mask
                            nucleus_mask = (MAX_NUC_LABELS == nucleus)
                            #Obtain nucleus max/min signals
                            # P_MAX = np.max(MED[PUNCTA_CHANNEL,:,:][nucleus_mask == True])
                            # P_MIN = np.min(MED[PUNCTA_CHANNEL,:,:][nucleus_mask == True])
                            # A_MAX = np.max(MED[ACETYL_CHANNEL,:,:][nucleus_mask == True])
                            # A_MIN = np.min(MED[ACETYL_CHANNEL,:,:][nucleus_mask == True])
                            # D_MAX = np.max(MED[DAPI_CHANNEL,:,:][nucleus_mask == True])
                            # D_MIN = np.min(MED[DAPI_CHANNEL,:,:][nucleus_mask == True])
                            #Obtain nucleus median signals
                            nm3d = np.array([nucleus_mask for _ in range(IMG.shape[1])])
                            
                            #Obtain median background levels
                            P_MED = np.mean(IMG[PUNCTA_CHANNEL,:,:,:][nm3d == True])
                            A_MED = np.mean(IMG[ACETYL_CHANNEL,:,:,:][nm3d == True])
                            D_MED = np.mean(IMG[DAPI_CHANNEL,:,:,:][nm3d == True])

                            #Generate random point
                            (rz, ry, rx) = pipeline_utilities.generate_random_coordinate(
                                IMG[0,:,:,:],
                                nucleus_mask,
                                MAX_PUNCTA_LABELS != 0,
                                PadLateral = PUNCTA_RADIUS_LATERAL,
                                PadZ = PUNCTA_RADIUS_AXIAL,
                            )
                            REMOTE_POINT_MAP[ry,rx] = 255
                            #Analyize signal at puncta
                            PS = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                PUNCTA_CHANNEL, 
                                px, 
                                py, 
                                pz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            if len(np.unique(PS)) <= 1: #ensure signal actually at puncta
                                continue
                            PS = PS/P_MED
                            #PS = pipeline_utilities.normalize_range(PS, P_MAX, P_MIN)
                            PUNCTA_CROPS.append(PS)
                            AS = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                ACETYL_CHANNEL, 
                                px, 
                                py, 
                                pz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            AS = AS/A_MED
                            #AS = pipeline_utilities.normalize_range(AS, A_MAX, A_MIN)
                            ACETYL_CROPS.append(AS)
                            DS = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                DAPI_CHANNEL, 
                                px, 
                                py, 
                                pz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            DS = DS/D_MED
                            #DS = pipeline_utilities.normalize_range(DS, D_MAX, D_MIN)
                            DAPI_CROPS.append(DS)

                            #Analyize remote signal
                            PS_R = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                PUNCTA_CHANNEL, 
                                rx, 
                                ry, 
                                rz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            PS_R = PS_R/P_MED
                            #PS_R = pipeline_utilities.normalize_range(PS_R, P_MAX, P_MIN)
                            PUNCTA_CROPS_REMOTE.append(PS_R)
                            AS_R = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                ACETYL_CHANNEL, 
                                rx, 
                                ry, 
                                rz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            AS_R = AS_R/A_MED
                            #AS_R = pipeline_utilities.normalize_range(AS_R, A_MAX, A_MIN)
                            ACETYL_CROPS_REMOTE.append(AS_R)
                            DS_R = pipeline_utilities.crop_image_to_coordinate(
                                IMG, 
                                DAPI_CHANNEL, 
                                rx, 
                                ry, 
                                rz, 
                                PUNCTA_RADIUS_LATERAL, 
                                PUNCTA_RADIUS_AXIAL,
                            )
                            DS_R = DS_R/D_MED
                            #DS_R = pipeline_utilities.normalize_range(DS_R, A_MAX, A_MIN)
                            DAPI_CROPS_REMOTE.append(DS_R)

                            #Store puncta data
                            puncta_data = puncta_data.append({
                                'File Index':file_inx,
                                'Condition':condition_name,
                                'Puncta ID':region.label,
                                'Nucleus ID':nucleus,
                                'CentroidX':cx,
                                'CentroidY':cy,
                                'PixelX':px,
                                'PixelY':py,
                                'PixelZ':pz,
                                'RemoteX':rx,
                                'RemoteY':ry,
                                'RemoteZ':rz,
                                'Area':region.area,
                            }, ignore_index=True)
                #Save puncta signal for condition
                if len(PUNCTA_CROPS) != 0:
                    print('Saving puncta images')
                    skio.imsave(OUT_DIR + 'puncta.tif', np.array(PUNCTA_CROPS), check_contrast=False)
                    skio.imsave(OUT_DIR + 'acetyl.tif', np.array(ACETYL_CROPS), check_contrast=False)
                    skio.imsave(OUT_DIR + 'dapi.tif', np.array(DAPI_CROPS), check_contrast=False)
                    skio.imsave(OUT_DIR + 'puncta_remote.tif', np.array(PUNCTA_CROPS_REMOTE), check_contrast=False)
                    skio.imsave(OUT_DIR + 'acetyl_remote.tif', np.array(ACETYL_CROPS_REMOTE), check_contrast=False)
                    skio.imsave(OUT_DIR + 'dapi_remote.tif', np.array(DAPI_CROPS_REMOTE), check_contrast=False)
    #Save punctae data
    print('Saving puncta data')
    puncta_data.to_csv(OUTPUT_DIRECTORY + 'punctae_data.csv')