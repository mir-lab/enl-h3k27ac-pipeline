# ENL H3K27Ac Pipeline

## Requirements

- Python 3+
    - Packages: numpy, scipy, scikit-image, pandas
    - For Carl Zeiss Image format: czifile
- 64 bit x86 processor
- 8+ GB RAM
- Tested on: Windows 10, MacOS Catalina
